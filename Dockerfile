FROM ubuntu:18.04

ENV TZ America/Belem

ENV DEBIAN_FRONTEND noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y libmysqlclient20:amd64 supervisor apt-transport-https \
    unzip wget gnupg  \
    locales rsync \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG=en_US.UTF-8

RUN useradd -ms /bin/bash archivematica

RUN wget -O - https://packages.archivematica.org/1.13.x/key.asc | apt-key add -

RUN echo "deb [arch=amd64] http://packages.archivematica.org/1.13.x/ubuntu bionic main" >> /etc/apt/sources.list \
    && echo "deb [arch=amd64] http://packages.archivematica.org/1.13.x/ubuntu-externals bionic main" >> /etc/apt/sources.list

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y archivematica-storage-service=1:0.19.0-1~18.04 \
    && rm -rf /var/lib/apt/lists/*

RUN echo "#!/bin/bash" > /usr/lib/archivematica/storage-service/migration.sh \
    && echo "/usr/share/archivematica/virtualenvs/archivematica-storage-service/bin/python manage.py migrate" >> /usr/lib/archivematica/storage-service/migration.sh \
    && chmod +x /usr/lib/archivematica/storage-service/migration.sh

WORKDIR /opt/archivematica

RUN chown -R archivematica:archivematica /opt/archivematica

USER archivematica

COPY ./supervisord.conf .

ENTRYPOINT ["/usr/bin/supervisord","-c","/opt/archivematica/supervisord.conf"]
